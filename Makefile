LEASOT_INSTALLED := $(shell leasot -v 2> /dev/null)
inventory_file = sangria-local.ini

print-setup:
	  ansible -i $(inventory_file) -m setup plex

plex-check:
	ansible-playbook -i $(inventory_file) plays/plex-server-setup.yml --diff --check

plex-run:
	ansible-playbook -i $(inventory_file) plays/plex-server-setup.yml --become

monitoring-check:
	ansible-playbook -i $(inventory_file) plays/monitoring-server-setup.yml --diff --check

monitoring-run:
	ansible-playbook -i $(inventory_file) plays/monitoring-server-setup.yml --become

show-todos:
	ifndef LEASOT_INSTALLED
	    $(error "leasot is not available install with: npm install -g leasot")
	leasot --exit-nicely -t .yaml plays/**/*.yml roles/**/*.yml
