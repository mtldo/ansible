#!/bin/bash
# This requires angle-grinder https://github.com/rcoh/angle-grinder

sudo journalctl -u docker-radarr -r --no-pager --since "1 hour ago" | agrind '"Not enough free space" |parse "/downloads/completed/*" as missing_movie| count by missing_movie'
missing_movie
